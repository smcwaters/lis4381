# LIS4381 - Mobile Web Application Development

## Shannon McWaters

### Project 1 # Requirements:

*Three Parts:*

1. My Business Card Mobile App
2. Chapter Questions (7, 8)
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshots of running mobile app
* Screenshots of Java skill sets

#### Assignment Screenshots:

*Screenshots of My Business Card Mobile App:*

| *Splash Screen:* | *Information Screen:* |
|------| |------|
| ![Splash Screen](img/splash.PNG) | ![Information Screen](img/contact.PNG) |

*Screenshots of Java Skill Sets:*

| *Skill Set 7: Random Array Using Methods and Data Validation* | *Skill Set 8: Largest of Three Integers* | *Skill Set 9: Arrays and Loops* |
|----| |----| |----|
| ![Skill Set 7](img/ss7.PNG) | ![Skill Set 8](img/ss8.PNG) | ![Skill Set 9](img/ss9.PNG) |
