> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Shannon McWaters

### Assignment 2 # Requirements:

*Three Parts:*

1. Healthy Recipe Mobile App
2. Chapter Questions (Chs 3, 4)
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshots of running mobile app
* Screenshots of Java skill sets

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of Healthy Recipes Mobile App:*

| *Splash Screen:* | *Ingredients Screen:* |
|------| |------|
| ![Splash Screen](img/homepage.PNG) | ![Ingredients Screen](img/recipepage.PNG) |

*Screenshots of Java Skill Sets:*

| *Skill Set 1: Even or Odd* | *Skill Set 2: Largest of Two Integers* | *Skill Set 3: Arrays and Loops* |
|----| |----| |----|
| ![Skill Set 1](img/ss1.PNG) | ![Skill Set 2](img/ss2.PNG) | ![Skill Set 3](img/ss3.PNG) |
