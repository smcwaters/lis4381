> **NOTE:** This README.md file should be *modified* and placed at the **root of *each* of your repos directories.**

# LIS4381 - Mobile Web Application Development

## Shannon McWaters

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](A1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app
    - Provide screenshots of completed Java skill sets
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD based upon business rules
    - Provide screenshot of completed ERD
    - Provide DB resource links
    - Create Concert Tickets Android app
    - Provide screenshots of completed app
    - Provide screenshots of completed Java skill sets
4. [A4 README.md](a4/README.md "My A4 README.md file")
5. [A5 README.md](a5/README.md "My A5 README.md file")
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create My Business Card Andorid app
    - Provide screenshots of completed app
    - Provide screenshots of completed Java skill sets
7. [P2 README.md](p2/README.md "My P2 README.md file")
